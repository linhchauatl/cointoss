//
//  CoinTossViewController.h
//  Coin Toss
//
//  Created by Linh Chau on 4/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoinTossViewController : UIViewController {
    __weak UILabel *status;
    UILabel *result;
}

@property (weak, nonatomic) IBOutlet UILabel *status;
@property (nonatomic, retain) IBOutlet UILabel *result;

- (IBAction)callHeads;
- (IBAction)callTails;

@end
