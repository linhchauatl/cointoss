//
//  CoinTossViewController.m
//  Coin Toss
//
//  Created by Linh Chau on 4/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CoinTossViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface CoinTossViewController ()

@end

@implementation CoinTossViewController
@synthesize status;
@synthesize result;

- (void) simulateCoinToss:(BOOL)userCalledHeads {
    BOOL coinLandedOnHeads = (arc4random() % 2) == 0;
    NSString * strResult = coinLandedOnHeads ? @"Heads" : @"Tails";
    
    result.text = [@"Result is: " stringByAppendingString:strResult];
    if (coinLandedOnHeads == userCalledHeads) {
        status.text = @"Correct!";
        result.textColor = [UIColor greenColor];
    }
    else {
        status.text = @"Wrong!";
        result.textColor = [UIColor redColor];
    }
    CABasicAnimation *rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    
    rotation.timingFunction = [CAMediaTimingFunction
                               functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    rotation.fromValue = [NSNumber numberWithFloat:0.0f];
    rotation.toValue = [NSNumber numberWithFloat:720 * M_PI / 180.0f];
    rotation.duration = 2.0f;
    
    [status.layer addAnimation:rotation forKey:@"rotate"];
    
    CABasicAnimation *fade = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fade.timingFunction = [CAMediaTimingFunction
                           functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    fade.fromValue = [NSNumber numberWithFloat:0.0f];
    fade.toValue = [NSNumber numberWithFloat:1.0f];
    fade.duration = 3.5f;
    [status.layer addAnimation:fade forKey:@"fade"];
}

- (IBAction) callHeads {
    [self simulateCoinToss:YES];
}

- (IBAction) callTails {
    [self simulateCoinToss:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    
    [self setStatus:nil];
    [super viewDidUnload];
    self.status = nil;
    self.result = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
